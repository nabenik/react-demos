// seccion (titulo, listado, seccion (titulo, listado de inst))

let langs = [
    {name: 'JavaScript', year: 1995},
    {name: 'CoffeeScript', year: 2009},
    {name: 'Dart', year: 2011},
    {name: 'TypeScript', year: 2012},
    {name: 'Kotlin', year: 2016}
]
let instrucciones = [
    "Instalar un interprete",
    "Instalar un editor de texto",
    "Estudiar POO y PF",
    "Practicar mucho"
]

function LangsComponent({
    title = "Ecosistema de programacion",
    langs,
    instrucciones}){
    
    return( <>
                <h1>{title}</h1>
                <ul>
                    { langs.map((lang,i) => <li key={i}>{lang.name}</li> ) }
                </ul>
                <section id="instrucciones">
                    <h2>¿Como aprendo {title}?</h2>
                    { instrucciones.map((instruccion,j) => <p key={j}>{instruccion}</p> ) }
                </section>
            </>);
}

//2- Utilizar el renderer adecuado
ReactDOM.render(
    <>
        <LangsComponent title="Ecosistema de JS" langs={langs} instrucciones={instrucciones}></LangsComponent>
        <hr/>
        <LangsComponent title="Ecosistema de .net "
            langs= {[{name:"C#", year:2000}, {name:"BASIC", year: 1964}]}
            instrucciones = { ["Instalar Windows", "Instalar .net core", "Estudiar MSDocs"] }
        ></LangsComponent>
    </>
, document.getElementById("root"))