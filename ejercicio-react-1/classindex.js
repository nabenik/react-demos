// seccion (titulo, listado, seccion (titulo, listado de inst))

let langs = [
    {name: 'JavaScript', year: 1995},
    {name: 'CoffeeScript', year: 2009},
    {name: 'Dart', year: 2011},
    {name: 'TypeScript', year: 2012},
    {name: 'Kotlin', year: 2016}
]
let instrucciones = [
    "Instalar un interprete",
    "Instalar un editor de texto",
    "Estudiar POO y PF",
    "Practicar mucho"
]

class LangsComponent extends React.Component{

    render(){
        let titleJS = React.createElement("h1", null, this.props.title)

        let listaDialectosJS = React.createElement("ul", null, 
            this.props.langs.map((lang, i) => React.createElement("li", {key: i}, lang.name))
        )
        
        let instruccionesJS = React.createElement("section", {id:"instrucciones"},
            React.createElement("h2", null, "¿Como aprendo JS"),
            this.props.instrucciones.map((instruccion, i) => React.createElement("p", {key:i}, instruccion))
        )
        
        let sectionReact = React.createElement(
            "section",
            { id: "reaction", className: "estilo1"},
            titleJS,
            listaDialectosJS,
            instruccionesJS
        )
        return sectionReact //ReactElement 
    }
}


//2- Utilizar el renderer adecuado
ReactDOM.render(
    <h1> hola mundo </h1>
, document.getElementById("root"))

/*[
        React.createElement(LangsComponent, {title: "Ecosistema de JS", langs: langs, instrucciones: instrucciones}),
        React.createElement("hr", null),
        React.createElement(LangsComponent, {
            title: "Ecosistema de .net",
            langs: [{name:"C#", year:2000}, {name:"BASIC", year: 1964}],
            instrucciones: ["Instalar Windows", "Instalar .net core", "Estudiar MSDocs"]})
    ]*/