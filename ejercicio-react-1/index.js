// seccion (titulo, listado, seccion (titulo, listado de inst))

let langs = [
    {name: 'JavaScript', year: 1995},
    {name: 'CoffeeScript', year: 2009},
    {name: 'Dart', year: 2011},
    {name: 'TypeScript', year: 2012},
    {name: 'Kotlin', year: 2016}
]
let instrucciones = [
    "Instalar un interprete",
    "Instalar un editor de texto",
    "Estudiar POO y PF",
    "Practicar mucho"
]

let title = React.createElement("h1", null, "El ecosistema de JS")

let listaDialectosJS = React.createElement("ul", null, 
    langs.map(lang => React.createElement("li", null, lang.name))
)

let instruccionesJS = React.createElement("section", {id:"instrucciones"},
    React.createElement("h2", null, "¿Como aprendo JS"),
    instrucciones.map(instruccion => React.createElement("p", null, instruccion))
)

let sectionReact = React.createElement(
    "section",
    { id: "reaction", className: "estilo1"},
    title,
    listaDialectosJS,
    instruccionesJS
)
//2- Utilizar el renderer adecuado
ReactDOM.render(sectionReact, document.getElementById("root"))