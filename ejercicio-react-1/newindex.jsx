function MPComponent({dummytext = "Lorem Ipsum", pais = "Guatemala"}){
    return <h1>Hola! {dummytext} bienvenido a {pais}</h1>
}

ReactDOM.render(
    <>
        <MPComponent dummytext="Este es un placeholder" pais="Mexico"></MPComponent>
        <MPComponent dummytext="React JS Rules!" pais="Peru" ></MPComponent>
        <MPComponent></MPComponent>
    </>
, document.getElementById("root"))