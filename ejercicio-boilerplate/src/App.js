import './App.css';
import NewRating from './gui/Rating';

function App() {
  console.log('Punto de depuracion');
  return (
    <>
      <div>Hola amigos del MP</div>
      <NewRating 
        totalLogos={7}
        style={{backgroundColor:'cyan'}}
        onDoubleClick={e => alert('JavaScript es genial!')}
      ></NewRating>
    </>
  );
}

export default App;
