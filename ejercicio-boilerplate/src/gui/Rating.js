import { IoLogoJavascript } from "react-icons/io5";
import React, { useState } from 'react';

/*export default function Rating(){
    return [
        <IoLogoJavascript color="yellow"/>,
        <IoLogoJavascript color="yellow"/>,
        <IoLogoJavascript color="yellow"/>,
        <IoLogoJavascript color="gray"/>,
        <IoLogoJavascript color="gray"/>
    ]
}*/
const Logo = ({ selected = false, onSelect }) => (
    <IoLogoJavascript color={selected?"yellow":"grey"} onClick={onSelect} />
);

//Hooks
export default function NewRating({style = {}, totalLogos = 5, ...props}){

    const [logosSelected, setSelectedLogos] = useState(0)

    return(<div style={{ padding: "10px", ...style}} {...props}>
        {[...Array(totalLogos)].map((n,i) => (
            <Logo
                key={i}//4
                selected={i < logosSelected}//4 < cant logos selec
                onSelect={() => {
                    console.log('Alguien ha hecho click!')
                    setSelectedLogos(i + 1);//4 + 1
                }}
            >
            </Logo>
        ))}
        <p> Seleccionados: {logosSelected} de {totalLogos} </p>
    </div>);
}


/*
export default class ClassicRating extends Component {
    constructor(props){
        super(props);
        this.state = {
            logosSelected: 0
        }
        this.change = this.change.bind(this)
    }

    change(logosSelected){
        this.setState({
            logosSelected
        })
    }

    render(){
        const { totalLogos } = this.props;
        const { logosSelected } = this.state;

        return(<div>
            {[...Array(totalLogos)].map((n,i) => (
                <Logo
                    key={i}//4
                    selected={i < logosSelected}//4 < cant logos selec
                    onSelect={() => {
                        console.log('Alguien ha hecho click!')
                        this.change(i + 1);//4 + 1
                    }}
                >
                </Logo>
            ))}
            <p> Seleccionados: {logosSelected} de {totalLogos} </p>
        </div>);
    }
}
*/
