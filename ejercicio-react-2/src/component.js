import React from "react";

function MPComponent({title="Ministerio público de Guatemala"}){
    return(<h1>{title}</h1>)
};

export default MPComponent;