import './App.css';
import {useState} from 'react';
import languagesData from './data/rating-data.json';
import LanguageList from './comp/LanguageList';
import AddLanguageForm from './comp/AddControlledLanguageForm';
import { v4 } from "uuid";

function App() {
  const [languages, setLanguages] = useState(languagesData);
  return (
    <>
      <AddLanguageForm

        onNewLanguage={ (title, color) => {
          //Agregar a listado existente
          const newLanguages = [
            ...languages,
            {
              id: v4(),
              rating: 0,
              title,
              color
            }
          ];
          setLanguages(newLanguages);
        }}
      />
      <LanguageList 
        languages={languages}
        onRemoveLanguage={id => {
            const newLanguages = languages.filter(language => language.id !== id)
            setLanguages(newLanguages)
        }}
        onRateLanguage ={ (id, rating) => {
          const newLanguages = languages.map(language => {
            return language.id === id ? {...language,rating} : language
          })
          setLanguages(newLanguages)
        }}
      
      ></LanguageList>
    </>
  );
}

export default App;
