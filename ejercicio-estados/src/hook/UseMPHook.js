
//value={title}
//onChange={ev => setTitle(ev.target.value)}
import { useState } from "react";

export const useMPHook = (initialValue) => {
    //Contenido del hook
    const [value, setValue] = useState(initialValue)

    return [ {value, onChange: e => setValue(e.target.value)},
        () => setValue(initialValue),
        () => setValue("")
    ]
}