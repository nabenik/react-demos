import Rating from "./Rating";
import {FaTrashAlt} from "react-icons/fa";

export default function Language({id, title, color, rating, onRemove, onRate}){

    return (
        <section>
            <h1>{title}</h1>
            <button onClick={ ()=>onRemove(id) }>
                <FaTrashAlt/>
            </button>
            <div style={{backgroundColor: color, height:40}} ></div>
            <Rating 
            selectedLogos={rating}
            onRate={rating => onRate(id, rating)}
            ></Rating>
        </section>
    );

}