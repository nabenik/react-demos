import Language from "./Language";

export default function LanguageList({languages = [], onRemoveLanguage, onRateLanguage}){
    if(!languages.length) return <div>Listado de datos vacio!</div>
    return (
        <div>
            {
                languages.map(language =>
                <Language
                    key={language.id} {...language}
                    onRemove={onRemoveLanguage}
                    onRate={onRateLanguage} ></Language>)
            }
        </div>
    )
}