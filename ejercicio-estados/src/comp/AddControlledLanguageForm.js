import React, { useState } from "react";
import { useMPHook } from "../hook/UseMPHook"

//Controlled components
export default function AddLanguageForm({onNewLanguage}){
    const [titleProps, resetTitle, emptyTitle] = useMPHook("");
    const [colorProps, resetColor, emptyColor] = useMPHook("");

    const submit = event => {
        event.preventDefault();
        onNewLanguage(titleProps.value, colorProps.value);
        resetTitle();
        resetColor();
    };

    return (
        <form onSubmit={submit}>
            <input 
            {...titleProps}
            type="text" placeholder="nombre del lenguaje..." required>
            </input>
            <input
            {...colorProps}
            type="color" required></input>
            <button>Agregar</button>
        </form>
    )
}