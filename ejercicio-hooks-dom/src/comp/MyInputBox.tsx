import React, { useState, useEffect } from "react";

export default function MyInputBox(){

    const [phrase, setPhrase] = useState("Frase predeterminada")
    const [val, set] = useState("")
    const createPhrase = () =>{
        setPhrase(val)
        set("")
    }

    useEffect(() => {
        console.log(`valor actual de val ${val}`)
    }, [val])

    useEffect(() => {
        console.log(`valor actual de phrase ${phrase}`)
    }, [phrase])

    useEffect(() => {
        console.log(`Evento que reacciona ante varios cambios`)
    }, [val, phrase])

    useEffect(() => {
        console.log(`Bootstrap`)
    }, [])

    return (
        <>
            <label>Mi frase favorita:</label>
            <input
                value={val}
                placeholder={phrase}
                onChange={(e:React.ChangeEvent<HTMLInputElement>) => set(e.target.value)}
             />
            <button onClick={createPhrase} >Aceptar</button>
        </>
    )

}