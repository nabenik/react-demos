import { useState,  useEffect} from "react";

let cuenta = 0;
//- Side effects
export default function MyCheckbox(){
    const [activo, setActivo] = useState(false);

    useEffect(()=>{
        alert(`activo: ${activo.toString()}`)
    })
    
    return (
        <>
            <input type="checkbox"
            value={activo.toString()}
            onChange={() => {
                cuenta++;
                setActivo(activo => !activo)
                console.log(cuenta)
            }
            }></input>
            {activo?"activo":"inactivo"}
        </>
    )
}