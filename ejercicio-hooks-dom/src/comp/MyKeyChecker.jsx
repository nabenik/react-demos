import { useState, useEffect } from "react";

export default function useMyKeyChecker(){

    const [, forceRenderer] = useState();

    useEffect(() =>{
        window.addEventListener("keydown", forceRenderer);
        return () => window.removeEventListener("keydown", forceRenderer);
    }, []);
}