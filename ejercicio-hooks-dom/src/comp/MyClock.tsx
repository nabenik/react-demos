import { useState } from "react";

export default function MyClock(){
    const [date, setDate] = useState(new Date().toLocaleDateString());
    const [time, setTime] = useState(new Date().toLocaleTimeString());

    setInterval(function(){
        setDate(new Date().toLocaleDateString());
        setTime(new Date().toLocaleTimeString());
      },1000)

    return(
        <>
        <h1>Hoy es: {date}</h1>
        <h2>Hora actual: {time}</h2>
        </>
    )
}