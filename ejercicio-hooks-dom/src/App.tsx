import React, { useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import MyInputBox from './comp/MyInputBox';
import useMyKeyChecker from './comp/MyKeyChecker';

let cuenta = 0

function App() {
  useMyKeyChecker();

  useEffect(() =>{
    console.log("El usuario acaba de teclear " + cuenta++);
  })

  return (
    <>
      <h1>Por favor vea la consola</h1>
    </>
  );
}

export default App;
