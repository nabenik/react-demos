import logo from './logo.svg';
import './App.css';
import GitHubUser from './comp/GitHubUser';

function App() {
  return (
    <GitHubUser username="tuxtor" />
  );
}

export default App;
