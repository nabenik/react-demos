import { useState, useEffect } from "react"; 

export default function GitHubUser({ username = 'torvalds'}){

    const [data, setData] = useState(); 

    useEffect( () => {
        if(!username) return;
        fetch(`https://api.github.com/users/${username}`) 
            .then(response => response.json())
            .then(setData)
            .catch(console.error);
    }, username)
    
    //Ya tengo los datos
    if(data)
        return (
            <div>
                <img
                    src={data.avatar_url}
                    style={{width: 250}}
                ></img>
                <div>
                    <h1>{username}</h1>
                    <p>{data.name}</p>
                </div>
            </div>
        );

    // No hay nada
    return (<h1>Aun sin informacion</h1>);
    
}