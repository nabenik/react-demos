import './App.css';
import LanguageList from './comp/LanguageList';
import AddLanguageForm from './comp/AddControlledLanguageForm';

function App() {
  return (
    <>
      <AddLanguageForm />
      <LanguageList />
    </>
  );
}

export default App;
