import Rating from "./Rating";
import {FaTrashAlt} from "react-icons/fa";
import { useLanguages } from "../data/LanguageProvider"

export default function Language({id, title, color, rating}){

    const { rateLanguage, removeLanguage } = useLanguages();

    return (
        <section>
            <h1>{title}</h1>
            <button onClick={ ()=>removeLanguage(id) }>
                <FaTrashAlt/>
            </button>
            <div style={{backgroundColor: color, height:40}} ></div>
            <Rating 
            selectedLogos={rating}
            onRate={rating => rateLanguage(id, rating)}
            ></Rating>
        </section>
    );

}