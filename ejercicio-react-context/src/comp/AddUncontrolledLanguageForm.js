import React, { useRef } from "react";

//Uncontrolled components
export default function AddLanguageForm({onNewLanguage}){
    const txtTitle = useRef();
    const hexColor = useRef();

    const submit = event => {
        event.preventDefault();
        const title = txtTitle.current.value;
        const color = hexColor.current.value;
        onNewLanguage(title, color);
        txtTitle.current.value = "";
        hexColor.current.value = "";
    };

    return (
        <form onSubmit={submit}>
            <input ref={txtTitle} type="text" placeholder="nombre del lenguaje..." required>
            </input>
            <input ref={hexColor} type="color" required></input>
            <button>Agregar</button>
        </form>
    )
}