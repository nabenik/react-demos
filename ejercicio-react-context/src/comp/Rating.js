import { IoLogoJavascript } from "react-icons/io5";

const Logo = ({ selected = false, onSelect }) => (
    <IoLogoJavascript color={selected?"orange":"grey"} onClick={onSelect} />
);

export default function Rating({style = {}, totalLogos = 5, selectedLogos=0, onRate,  ...props}){
    return(<div style={{ padding: "10px", ...style}} {...props}>
        {[...Array(totalLogos)].map((n,i) => (
            <Logo
                key={i}
                selected={i < selectedLogos}
                onSelect={() => {
                    onRate(i+1)
                }}
            >
            </Logo>
        ))}
        <p> Seleccionados: {selectedLogos} de {totalLogos} </p>
    </div>);
}