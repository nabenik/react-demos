import Language from "./Language";
import React, { useContext } from "react";
import { useLanguages }  from "../data/LanguageProvider"

export default function LanguageList(){

    const { languages } = useLanguages();

    if(!languages.length) return <div>Listado de datos vacio!</div>
    
    return (
        <div>
            {
                languages.map(language =>
                <Language
                    key={language.id} {...language} ></Language>)
            }
        </div>
    )
}