import React, { useState, useContext } from "react";
import { useMPHook } from "../hook/UseMPHook"
import { useLanguages } from "../data/LanguageProvider"
import { v4 } from "uuid";

//Controlled components
export default function AddLanguageForm(){
    const [titleProps, resetTitle, emptyTitle] = useMPHook("");
    const [colorProps, resetColor, emptyColor] = useMPHook("");

    const { addLanguage } = useLanguages();

    const submit = event => {
        event.preventDefault();
        addLanguage(titleProps.value, colorProps.value)
        resetTitle();
        resetColor();
    };

    return (
        <form onSubmit={submit}>
            <input 
            {...titleProps}
            type="text" placeholder="nombre del lenguaje..." required>
            </input>
            <input
            {...colorProps}
            type="color" required></input>
            <button>Agregar</button>
        </form>
    )
}