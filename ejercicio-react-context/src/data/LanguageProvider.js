//Stateful context provider
// 1- Mantener el estado entre renders
// 2- Contexto central
// 3- Operaciones que mutan la información en el contexto

import React, { useState, createContext, useContext } from "react";
import languagesData from './rating-data.json';
import { v4 } from "uuid";

const LanguageContext = createContext();
export const useLanguages = () => useContext(LanguageContext);

export default function LanguageProvider( { children } ) {

    const [languages, setLanguages] = useState(languagesData);

    //1- add
    const addLanguage = (title, color) => setLanguages([
        ...languages,
        {
            id: v4(),
            rating: 0,
            title,
            color
        }
    ]);

    //2- calificar (rate)
    const rateLanguage = (id, rating) => 
        setLanguages(
            languages.map(language => ( language.id === id ? {...language, rating}  : language))
        );

    //3- eliminar (remove)
    const removeLanguage = id => 
        setLanguages( languages.filter(language => language.id !== id) );

    return(
        <LanguageContext.Provider value={{languages, addLanguage, removeLanguage, rateLanguage}}>
            { children }
        </LanguageContext.Provider>
    )

}