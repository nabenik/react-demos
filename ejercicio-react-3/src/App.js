import React, { useState } from "react";
import colorData from './gui/color-data.json'
import ColorList from "./gui/ColorList";
import AddColorForxmImperative from "./gui/AddColorFormImperative";
import AddColorForm from "./gui/AddColorForm";
import { v4 } from 'uuid';
function create_UUID(){
  var dt = new Date().getTime();
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = (dt + Math.random()*16)%16 | 0;
      dt = Math.floor(dt/16);
      return (c=='x' ? r :(r&0x3|0x8)).toString(16);
  });
  return uuid;
}

function App() {
  return (
    <>
      <AddColorForm></AddColorForm>
      <ColorList></ColorList>
    </>
  );
}

export default App;
