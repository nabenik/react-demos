import React, { Component } from "react";
import { FaStar } from 'react-icons/fa';

const Star = ({ selected = false, onSelect = f => f }) => ( 
    <FaStar color={selected ? "red" : "grey"} onClick={onSelect} />
);

export default class OldStarRating extends Component {
    constructor(props) {
      super(props);
      this.state = {
        starsSelected: 0
      };
      this.change = this.change.bind(this);
    }
    
    change(starsSelected) {
      this.setState({ starsSelected });
    }

    render() {
      const { totalStars } = this.props;
      const { starsSelected } = this.state;
      return (
        <div>
            {[...Array(totalStars)].map((n, i) => (
            <Star
              key={i}
              selected={i < starsSelected}
              onClick={() => this.change(i + 1)}
            />
            ))}
            <p>
                {starsSelected} de {totalStars} estrellas
            </p>
        </div> );
} }